package com.example.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.model.UniversitasModel;
import com.example.service.UniversitasService;

@Controller
public class UniversitasController {
	@Autowired
	UniversitasService univDAO;
	
	@RequestMapping("/univ")
	public String view(Model model)
	{
		List<UniversitasModel> universitass = univDAO.selectAllUniversitas();
		model.addAttribute("universitass", universitass);
		return "view-all-universitas";
	}
	
	@RequestMapping("/univ/{kodeUniv}")
    public String viewUnivById (Model model, @PathVariable(value = "kodeUniv") String kode_univ)
    {
        UniversitasModel universitasProdi = univDAO.selectUniversitasAndProdi(kode_univ);
        if (universitasProdi != null) {
            model.addAttribute ("universitasProdi", universitasProdi);
            return "view-detail-universitas";
        } else {
            model.addAttribute ("kode_univ", kode_univ);
            return "universitas-not-found";
        }
    }
}
