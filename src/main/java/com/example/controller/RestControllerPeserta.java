package com.example.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.model.PesertaModel;
import com.example.service.PesertaService;


@RestController
public class RestControllerPeserta {
	@Autowired
	PesertaService pesertaDAO;
	
	@RequestMapping(value = "/rest/peserta", method = { RequestMethod.GET  })
	 public @ResponseBody PesertaModel peserta(Model model, @RequestParam(value = "nomor", required = false) String nomor_peserta)
	 {
		PesertaModel peserta = pesertaDAO.selectPeserta(nomor_peserta);
		if(peserta != null){
			int umur = pesertaDAO.hitungUmur(peserta);
			model.addAttribute("peserta", peserta);
			model.addAttribute("umur", umur);
			return peserta;
		}
		else {
			model.addAttribute("nomor-peserta", nomor_peserta);
			return peserta;
		}
	 }
}
