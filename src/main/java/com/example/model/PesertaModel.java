package com.example.model;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PesertaModel {
	private String nomor;
	private String nama;
	private Date tgl_lahir;
	private String kode_prodi;
	
	public String getNomor(){
		return this.nomor;
	}
	
	public String getNama(){
		return this.nama;
	}
	
	public Date getTgl_lahir(){
		return this.tgl_lahir;
	}
}
