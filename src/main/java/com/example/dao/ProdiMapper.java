package com.example.dao;

import java.util.List;

import org.apache.ibatis.annotations.*;

import com.example.model.PesertaModel;
import com.example.model.ProdiModel;
import com.example.model.UniversitasModel;

@Mapper
public interface ProdiMapper {
		
	@Select("select * from prodi where kode_prodi = #{kode_prodi}")
	@Results(value = {
			@Result(property="kode_univ", column="kode_univ"),
			@Result(property="kode_prodi", column="kode_prodi"),
			@Result(property="nama_prodi", column="nama_prodi"),
			@Result(property="universitas", column="kode_univ", one=@One(select = "selectUniversitas")),
			@Result(property="peserta", column="kode_prodi", javaType=List.class, many=@Many(select="selectPeserta"))
	}) ProdiModel selectProdiUniversitasPeserta (@Param("kode_prodi") String kode_prodi);
	
	@Select("select * from univ where kode_univ = #{kode_univ}")
	UniversitasModel selectUniversitas(@Param("kode_univ") String kode_univ);
	
	@Select("select * from peserta where kode_prodi = #{kode_prodi}")
	List<PesertaModel> selectPeserta(@Param("kode_prodi") String kode_prodi);
}
