package com.example.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.model.PesertaModel;
import com.example.model.ProdiModel;
import com.example.service.PesertaService;
import com.example.service.ProdiService;

@RestController
public class RestControllerProdi {
	@Autowired
	ProdiService prodiDAO;
	@Autowired
	PesertaService pesertaDAO;
	
	@RequestMapping(value = "rest/prodi", method = { RequestMethod.GET  })
	public @ResponseBody ProdiModel getDetailProdi(@RequestParam(value = "kode") String kode_prodi, Model model) {
		ProdiModel prodiUnivPeserta = prodiDAO.selectProdiUniversitasPeserta(kode_prodi);
        if (prodiUnivPeserta != null) {
        	PesertaModel maxPeserta = prodiDAO.maxPeserta(prodiUnivPeserta.getPeserta());
        	PesertaModel minPeserta = prodiDAO.minPeserta(prodiUnivPeserta.getPeserta());
        	
        	int umurMaxPeserta = pesertaDAO.hitungUmur(maxPeserta);
        	int umurMinPeserta = pesertaDAO.hitungUmur(minPeserta);
        	
        	model.addAttribute ("maxPeserta", maxPeserta);
        	model.addAttribute ("umurMaxPeserta", umurMaxPeserta);
        	model.addAttribute ("minPeserta", minPeserta);
        	model.addAttribute ("umurMinPeserta", umurMinPeserta);
            model.addAttribute ("prodiUnivPeserta", prodiUnivPeserta);
            return prodiUnivPeserta;
        } else {
            //model.addAttribute ("kode_univ", kode_prodi);
            return prodiUnivPeserta;
        }
	}
}
