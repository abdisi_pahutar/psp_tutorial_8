package com.example.service;

import groovy.util.logging.Slf4j;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.dao.ProdiMapper;
import com.example.model.PesertaModel;
import com.example.model.ProdiModel;


@Slf4j
@Service
public class ProdiServiceDatabase implements ProdiService{

	@Autowired
	private ProdiMapper prodiMapper;

	@Override
	public ProdiModel selectProdiUniversitasPeserta(String kode_prodi) {
		return prodiMapper.selectProdiUniversitasPeserta(kode_prodi);
	}

	@Override
	public PesertaModel maxPeserta(List<PesertaModel> peserta) {
		PesertaModel tempMaxPeserta = peserta.get(0);
		int umur = 0;
		for(int i=1; i<peserta.size(); i++){
			umur = (getUmur(peserta.get(i)));
			
			if(umur > getUmur(tempMaxPeserta)){
				tempMaxPeserta = peserta.get(i);
			}
		}
		return tempMaxPeserta;
	}
	
	@Override
	public PesertaModel minPeserta(List<PesertaModel> peserta) {
		PesertaModel tempMinPeserta = peserta.get(0);
		int umur = 0;
		for(int i=1; i<peserta.size(); i++){
			umur = (getUmur(peserta.get(i)));
			
			if(umur < getUmur(tempMinPeserta)){
				tempMinPeserta = peserta.get(i);
			}
		}
		return tempMinPeserta;
	}

	@Override
	public int getUmur(PesertaModel peserta) {
		Date now = new Date();
        long diff = now.getTime() - peserta.getTgl_lahir().getTime();
        int umur = (int)(TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS))/365;
        return umur;
	}
}
