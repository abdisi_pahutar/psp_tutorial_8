package com.example.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;

import com.example.model.PesertaModel;
import com.example.model.ProdiModel;
import com.example.service.PesertaService;
import com.example.service.ProdiService;

@Controller
public class PesertaController {
	@Autowired
	PesertaService pesertaDAO;
	@Autowired
	ProdiService prodiDAO;
	
	@RequestMapping("/")
    public String index ()
    {
        return "index";
    }
	
	@RequestMapping("/pengumuman/submit")
	public String cek(Model model, @RequestParam(value = "nps", required = true) String nps) {
		PesertaModel peserta = pesertaDAO.selectPeserta(nps);
		if(peserta != null){
			int umur = pesertaDAO.hitungUmur(peserta);
			model.addAttribute("peserta", peserta);
			model.addAttribute("umur", umur);
			return "info-pengumuman";
		}
		else {
			model.addAttribute("nps", nps);
			return "not-found";
		}
	}
	
	@RequestMapping("/peserta")
	public String getDetailProdi(@RequestParam(value = "nomor") String nomor_peserta, Model model) {
		PesertaModel peserta = pesertaDAO.selectPeserta(nomor_peserta);
		if(peserta != null){
			int umur = pesertaDAO.hitungUmur(peserta);
			model.addAttribute("peserta", peserta);
			model.addAttribute("umur", umur);
			return "view-detail-peserta";
		}
		else {
			model.addAttribute("nomor-peserta", nomor_peserta);
			return "not-found";
		}
	}
}
