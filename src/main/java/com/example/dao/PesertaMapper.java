package com.example.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.annotations.Select;

import com.example.model.PesertaModel;

@Mapper
public interface PesertaMapper {

	@Select("SELECT * FROM peserta WHERE nomor= #{nomor}")
	@Results(value = {
			@Result(property="nomor", column="nomor"),
			@Result(property="nama", column="nama"),
			@Result(property="tgl_lahir", column="tgl_lahir"),
			@Result(property="kode_prodi", column="kode_prodi")
	})PesertaModel selectPeserta (@Param("nomor") String nomor);
}
