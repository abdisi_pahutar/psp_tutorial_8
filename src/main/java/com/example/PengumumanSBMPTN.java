package com.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PengumumanSBMPTN {

	public static void main(String[] args) {
		SpringApplication.run(PengumumanSBMPTN.class, args);
	}
}
