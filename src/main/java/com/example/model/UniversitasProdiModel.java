package com.example.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UniversitasProdiModel {
	private String kode_univ;
	private String nama_univ;
	private String url_univ;
	private String kode_prodi;
	private String nama_prodi;
}
