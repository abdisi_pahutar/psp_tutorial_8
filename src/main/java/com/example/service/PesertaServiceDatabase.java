package com.example.service;

import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.dao.PesertaMapper;
import com.example.model.PesertaModel;

import groovy.util.logging.Slf4j;

@Slf4j
@Service
public class PesertaServiceDatabase implements PesertaService{

	@Autowired
	private PesertaMapper pesertaMapper ;

	@Override
	public PesertaModel selectPeserta(String nomor) {
		// TODO Auto-generated method stub
		return pesertaMapper.selectPeserta(nomor);
	}
	
	@Override
    public int hitungUmur(PesertaModel peserta) {
        Date now = new Date();
        long diff = now.getTime() - peserta.getTgl_lahir().getTime();
        int umur = (int)(TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS))/365;
        return umur;
    }
}
