package com.example.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.example.model.PesertaModel;
import com.example.model.ProdiModel;
import com.example.service.PesertaService;
import com.example.service.ProdiService;


@Controller
public class ProdiController {
	@Autowired
	ProdiService prodiDAO;
	@Autowired
	PesertaService pesertaDAO;
	
	@RequestMapping("/prodi")
	public String getDetailProdi(@RequestParam(value = "kode") String kode_prodi, Model model) {
		ProdiModel prodiUnivPeserta = prodiDAO.selectProdiUniversitasPeserta(kode_prodi);
        if (prodiUnivPeserta != null) {
        	PesertaModel maxPeserta = prodiDAO.maxPeserta(prodiUnivPeserta.getPeserta());
        	PesertaModel minPeserta = prodiDAO.minPeserta(prodiUnivPeserta.getPeserta());
        	
        	int umurMaxPeserta = pesertaDAO.hitungUmur(maxPeserta);
        	int umurMinPeserta = pesertaDAO.hitungUmur(minPeserta);
        	
        	model.addAttribute ("maxPeserta", maxPeserta);
        	model.addAttribute ("umurMaxPeserta", umurMaxPeserta);
        	model.addAttribute ("minPeserta", minPeserta);
        	model.addAttribute ("umurMinPeserta", umurMinPeserta);
            model.addAttribute ("prodiUnivPeserta", prodiUnivPeserta);
            return "view-detail-prodi";
        } else {
            model.addAttribute ("kode_univ", kode_prodi);
            return "universitas-not-found";
        }
	}
}
