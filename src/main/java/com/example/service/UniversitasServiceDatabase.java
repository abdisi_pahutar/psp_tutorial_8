package com.example.service;

import groovy.util.logging.Slf4j;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.dao.UniversitasMapper;
import com.example.model.UniversitasModel;

@Slf4j
@Service
public class UniversitasServiceDatabase implements UniversitasService{

	@Autowired
	private UniversitasMapper universitasMapper;

	@Override
	public List<UniversitasModel> selectAllUniversitas() {
		return universitasMapper.selectAllUniversitas();
	}

	@Override
	public UniversitasModel selectUniversitasAndProdi(String kode_univ) {
		return universitasMapper.selectUniversitasAndProdi(kode_univ);
	}
}
