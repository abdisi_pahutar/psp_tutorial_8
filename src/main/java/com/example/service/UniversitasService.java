package com.example.service;

import java.util.List;

import com.example.model.UniversitasModel;

public interface UniversitasService {
	List<UniversitasModel> selectAllUniversitas();
	
	UniversitasModel selectUniversitasAndProdi(String kode_univ);
}