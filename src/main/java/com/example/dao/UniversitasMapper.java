package com.example.dao;

import java.util.List;

import org.apache.ibatis.annotations.*;

import com.example.model.ProdiModel;
import com.example.model.UniversitasModel;

@Mapper
public interface UniversitasMapper {
	@Select("select * from univ")
	@Results(value = {
			@Result(property="kode_univ", column="kode_univ"),
			@Result(property="nama_univ", column="nama_univ"),
			@Result(property="url_univ", column="url_univ")
	}) List<UniversitasModel> selectAllUniversitas ();
	
	@Select("select * from univ where kode_univ = #{kode_univ}")
	@Results(value = {
			@Result(property="kode_univ", column="kode_univ"),
			@Result(property="nama_univ", column="nama_univ"),
			@Result(property="url_univ", column="url_univ"),
			@Result(property="prodi", column="kode_univ", javaType=List.class, many=@Many(select="selectProdi"))
	}) UniversitasModel selectUniversitasAndProdi (@Param("kode_univ") String kode_univ);
	
	@Select("select * from prodi where kode_univ = #{kode_univ}")
	List<ProdiModel> selectProdi();
	
	@Update("Delete univ where kode_univ= #{kode_univ}")
	void deleteUniv(@Param("kode_univ") String kode_univ);
}
