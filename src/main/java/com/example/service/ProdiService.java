package com.example.service;

import java.util.ArrayList;
import java.util.List;

import com.example.model.PesertaModel;
import com.example.model.ProdiModel;

public interface ProdiService {
	ProdiModel selectProdiUniversitasPeserta(String kode_prodi);
	PesertaModel maxPeserta(List<PesertaModel> peserta);
	PesertaModel minPeserta(List<PesertaModel> peserta);
	int getUmur(PesertaModel peserta);
}